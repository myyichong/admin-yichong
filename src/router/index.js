import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index.vue'
import adop from '../views/adop.vue'
import shop from '../views/shop'
import Login from '../views/Login'
import Layout from '../views/Layout'
//威哥
import UserInfoPub from "@/views/UserInfoPub"
import UserInfoList from "@/views/UserInfoList"
import Time from "@/views/Time"
//cy
import list from '@/views/list'
import shower from '@/views/shower'



Vue.use(VueRouter)

const routes = [
  {
  path: '/login',
  component: Login
}, {
  path: '/pat',
  component: Layout,
  children: [{
      path: '/',
      component: index,
    },
    
    {
      path: 'del',
      component: adop
    },
    {
      path: 'detail',
      component: shop
    },
  ]
}, {
  path: "/",
  component: Layout,
  children: [{
      path: '/',
      component: UserInfoPub
    },
    {
      path: 'list',
      component: UserInfoList
    },
    {
      path: 'time',
      component: Time
    }
  ]
}, {
  path: '/shop',
  component: Layout,
  children: [ {
      path: '/',
      component: list
    },
    {
      path: 'shower',
      component: shower
    },
  ]
}]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to,from,next)=>{
  // console.log(localStorage.mobile)
    if(localStorage.mobile){
        next()
    }else{
      next()
        router.push({path:"/login"})
    }
})

export default router