import $http from '../http'

export const postuser = (url, data) => {
    return $http.post(url, data)
}
export const getuser = (url, data) => {
    return $http.get(url, {
            params: data
        }
    )
}