import axios from "axios";
import Vue from "vue";
import router from "@/router"
axios.defaults.withCredentials = true
const $axios =axios.create({
    baseURL:"http://localhost:3000"
})


// 添加请求拦截器
$axios.interceptors.request.use(function (config) {

  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
$axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  console.log(response);
  let message=response.data.msg
  // console.log(message)
  if(response.data.type==1){
      Vue.prototype.$message({
        message,
        type: 'success'
      })
  }

  if(response.data.type==0){
    Vue.prototype.$message.error({message})
  }
  if(response.data.type==-1){//token验证错误或者失效
    Vue.prototype.$message({
      message,
      type: 'warning'
    })
    router.push({
      path:"/login"
    })
  }
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default $axios