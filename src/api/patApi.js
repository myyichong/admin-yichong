import $http from './axios'
export const postPat = (url, data) => {
    return $http.post(url, data);
}
export const getPat = (url) => {
    return $http.get(url);
}

export const delPat = (url) => {
    return $http.delete(url);
}

export const postUser = (url, data) => {
    return $http.post(url, data);
}

//admin登录
export const getUser = (url, data) => {
    return $http.post(url,data);
}