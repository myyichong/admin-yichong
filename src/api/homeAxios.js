import $axios from "./axios";

export const getUserInfo=(url)=>{
    return $axios.get(url)
}

export const delUserInfo=(url,data)=>{
    return $axios.delete(url,data)
}


//发布数据
export const pubUserInfo=(url,data)=>{
    return $axios.post(url,data)
}
//向数据库保存用户预约时间
export const getTime=(url,data)=>{
    return $axios.post(url,data)
}

//向数据库获取用户注册的信息
export const getLogin=(url,data)=>{
    return $axios.post(url,data)
}