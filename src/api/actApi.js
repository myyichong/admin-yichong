import $http from './axios'

export const getact = (url) => {
    return $http.get(url)
}

export const delact = (url) => {
    return $http.delete(url)
}

export const postact = (url, data) => {
    return $http.post(url, data)
}